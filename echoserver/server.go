package main

import (
	"bufio"
	"log"
	"net"
	"os"
	"path/filepath"
	"time"
)

var listenAddr = ":8888"

const (
	noActivityTimeout = 30 * time.Minute
	writeLineTimeout  = 3 * time.Second
)

func main() {

	if len(os.Args) > 2 {
		log.Fatalf("Usage: %s [server:port]", filepath.Base(os.Args[0]))
	}
	if len(os.Args) == 2 {
		listenAddr = os.Args[1]
	}

	l, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatalln(err)
	}
	defer l.Close()

	log.Printf("Listening on %v", l.Addr())
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatalln(err)
		}
		go handleClient(conn)
	}
}

func handleClient(conn net.Conn) {

	defer conn.Close()

	remoteAddr := conn.RemoteAddr().String()
	log.Printf("connection started with %s", remoteAddr)
	defer log.Printf("connected ended with %s", remoteAddr)

	scanner := bufio.NewScanner(conn)

	for {
		if err := conn.SetReadDeadline(time.Now().Add(noActivityTimeout)); err != nil {
			log.Println(err)
			break
		}
		if !scanner.Scan() {
			if err := scanner.Err(); err != nil {
				log.Println(err)
			}
			break
		}
		echoBytes := scanner.Bytes()
		log.Printf("Echoing to %s: %s", remoteAddr, string(echoBytes))
		echoBytes = append(echoBytes, '\n')

		if err := conn.SetWriteDeadline(time.Now().Add(writeLineTimeout)); err != nil {
			log.Println(err)
			break
		}
		// A short write without an error being returned should not be possible
		if _, err := conn.Write(echoBytes); err != nil {
			log.Println(err)
			break
		}
	}
}
