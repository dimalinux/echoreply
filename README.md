# TCP Echo-Reply Server + Client in Golang

## Client
Client reads whole lines of standard input, sends them to
the server and waits for line to be echoed back.

## Server
Handles client connections.  Echos whole lines received back.


## Installation

Install client and server:
```
go get -u bitbucket.org/dimalinux/echoreply/...
```
Server only:
```
go get -u bitbucket.org/dimalinux/echoreply/echoserver
```
Client only:
```
go get -u bitbucket.org/dimalinux/echoreply/echoclient
```