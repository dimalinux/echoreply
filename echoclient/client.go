package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

const (
	echoRoundTripDeadline = time.Second * 3
	dialTimeout           = time.Second * 3
)

func main() {

	if len(os.Args) != 2 {
		exitOnError(fmt.Sprintf("Usage: %s server:port\n", filepath.Base(os.Args[0])))
	}
	serverAddr := os.Args[1]

	conn, err := net.DialTimeout("tcp", serverAddr, dialTimeout)
	exitIfError(err)
	defer conn.Close()

	eofKeys := "CNTRL-D"
	if runtime.GOOS == "windows" {
		eofKeys = "CNTRL-Z"
	}
	fmt.Printf("Connected to %s, %s ends session\n", conn.RemoteAddr(), eofKeys)

	inputScanner := bufio.NewScanner(os.Stdin)
	echoScanner := bufio.NewScanner(conn)

	for {
		fmt.Print("Input: ")

		// Read a new line of user input
		if !inputScanner.Scan() {
			exitIfError(inputScanner.Err())
			fmt.Println() // finish the input line if user hits control-D
			break
		}

		// Setting a single deadline for all blocking i/o calls in the echo/reply round trip
		err = conn.SetDeadline(time.Now().Add(echoRoundTripDeadline))
		exitIfError(err)

		// Write the text to the remote server. A non-error short write shouldn't be possible.
		_, err = conn.Write(append(inputScanner.Bytes(), '\n'))
		exitIfError(err)

		// Read back the echo
		if !echoScanner.Scan() {
			exitIfError(echoScanner.Err())
			exitOnError("Connection with echo server was closed")
		}
		fmt.Println("Echo :", echoScanner.Text())
	}
}

func exitIfError(err error) {
	if err != nil {
		exitOnError(err)
	}
}

func exitOnError(message interface{}) {
	fmt.Fprintln(os.Stderr, message)
	os.Exit(1)
}
